package com.audienceplusbi.microService.models;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="audience")
public class MobileAudience implements Serializable {
	
	@Id
	private String id;
	private String owner;
	private String queryID;
	private String registerID;
	private String advertisingID;
	private String osname;
	private String grpkey;
	private String grpvalue;
	private Boolean sent;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getQueryID() {
		return queryID;
	}
	public void setQueryID(String queryID) {
		this.queryID = queryID;
	}
	public String getRegisterID() {
		return registerID;
	}
	public void setRegisterID(String registerID) {
		this.registerID = registerID;
	}
	public String getAdvertisingID() {
		return advertisingID;
	}
	public void setAdvertisingID(String advertisingID) {
		this.advertisingID = advertisingID;
	}
	public String getOsname() {
		return osname;
	}
	public void setOsname(String osname) {
		this.osname = osname;
	}
	public String getGrpkey() {
		return grpkey;
	}
	public void setGrpkey(String grpkey) {
		this.grpkey = grpkey;
	}
	public String getGrpvalue() {
		return grpvalue;
	}
	public void setGrpvalue(String grpvalue) {
		this.grpvalue = grpvalue;
	}
	public Boolean getSent() {
		return sent;
	}
	public void setSent(Boolean sent) {
		this.sent = sent;
	}
	
	

}
