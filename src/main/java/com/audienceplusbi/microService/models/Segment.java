package com.audienceplusbi.microService.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="segment"
    ,catalog="audiencedi"
)

public class Segment implements Serializable {
	


	     private Integer id;
	     private String name;
	     private Date creationdate;
	     private Integer size;
	     private String type;
	     private Date lastupdate;
	     private Integer panel;
	     private String owner;
	     private String jsonsegment;

	    public Segment() {
	    }

		
	    public Segment(String name, Date creationdate, Integer panel, String owner) {
	        this.name = name;
	        this.creationdate = creationdate;
	        this.panel = panel;
	        this.owner = owner;
	    }
	    
	    public Segment(String name, Date creationdate, Integer size, String type, Date lastupdate, Integer panel, String owner, String jsonsegment) {
	       this.name = name;
	       this.creationdate = creationdate;
	       this.size = size;
	       this.type = type;
	       this.lastupdate = lastupdate;
	       this.panel = panel;
	       this.owner = owner;
	       this.jsonsegment = jsonsegment;
	    }
	   
	     @Id @GeneratedValue(strategy=IDENTITY)

	    
	    @Column(name="id", unique=true, nullable=false)
	    public Integer getId() {
	        return this.id;
	    }
	    
	    public void setId(Integer id) {
	        this.id = id;
	    }

	    
	    @Column(name="name", length=45)
	    public String getName() {
	        return this.name;
	    }
	    
	    public void setName(String name) {
	        this.name = name;
	    }

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name="creationdate", length=19)
	    public Date getCreationdate() {
	        return this.creationdate;
	    }
	    
	    public void setCreationdate(Date creationdate) {
	        this.creationdate = creationdate;
	    }

	    
	    @Column(name="size")
	    public Integer getSize() {
	        return this.size;
	    }
	    
	    public void setSize(Integer size) {
	        this.size = size;
	    }

	    
	    @Column(name="type", length=45)
	    public String getType() {
	        return this.type;
	    }
	    
	    public void setType(String type) {
	        this.type = type;
	    }

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name="lastupdate", length=19)
	    public Date getLastupdate() {
	        return this.lastupdate;
	    }
	    
	    public void setLastupdate(Date lastupdate) {
	        this.lastupdate = lastupdate;
	    }
	    
	    
	    @Column(name="panel")
	    public Integer getPanel() {
	        return this.panel;
	    }
	    
	    public void setPanel(Integer panel) {
	        this.panel = panel;
	    }
	    
	    @Column(name="owner", length=128)
	    public String getOwner() {
	        return this.owner;
	    }
	    
	    public void setOwner(String owner) {
	        this.owner = owner;
	    }

	    @Column(name="jsonsegment", length=65535)
	    public String getJsonsegment() {
	        return this.jsonsegment;
	    }
	    
	    public void setJsonsegment(String jsonsegment) {
	        this.jsonsegment = jsonsegment;
	    }

}
