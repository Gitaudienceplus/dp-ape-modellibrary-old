package com.audienceplusbi.microService.models;

import java.io.Serializable;
import java.util.List;

public class OutputEmailAudienceMS1 implements Serializable {
	private List<Segment> segmentList;
	private int cid;
	private String token;
	private String queryId;
	
	public List<Segment> getSegmentList() {
		return segmentList;
	}
	public void setSegmentList(List<Segment> segmentList) {
		this.segmentList = segmentList;
	}
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getQueryId() {
		return queryId;
	}
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	

}
