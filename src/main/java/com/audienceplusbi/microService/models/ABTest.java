package com.audienceplusbi.microService.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="audience")
@Entity
@Table(name="segmentabtest")
public class ABTest implements Serializable {

	private String owner;
	private String uniqueID;
	private String registerID;
	private int abtCode;
	private String abtGrp;
	private String osName;
	private String appName;
	private String publicKey;
	private String authKey;
	
	
	
	
	
	public ABTest(String owner, String uniqueID, String registerID, int abtCode, String abtGrp, String osName,
			String appName, String publicKey, String authKey) {
		super();
		this.owner = owner;
		this.uniqueID = uniqueID;
		this.registerID = registerID;
		this.abtCode = abtCode;
		this.abtGrp = abtGrp;
		this.osName = osName;
		this.appName = appName;
		this.publicKey = publicKey;
		this.authKey = authKey;
	}
	
	@Id

	@Column(name="UniqueID")
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	@Transient
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	@Column(name="RegisterID")
	public String getRegisterID() {
		return registerID;
	}
	public void setRegisterID(String registerID) {
		this.registerID = registerID;
	}
	@Column(name="abtcode")
	public int getAbtCode() {
		return abtCode;
	}
	public void setAbtCode(int abtCode) {
		this.abtCode = abtCode;
	}
	@Column(name="abtgrp")
	public String getAbtGrp() {
		return abtGrp;
	}
	public void setAbtGrp(String abtGrp) {
		this.abtGrp = abtGrp;
	}
	@Column(name="OSName")
	public String getOsName() {
		return osName;
	}
	public void setOsName(String osName) {
		this.osName = osName;
	}
	
	@Column(name="appname")
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	@Column(name="publickey")
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	@Column(name="authkey")
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
	
	

}
