package com.audienceplusbi.microService.models;

import java.io.Serializable;
import java.util.List;


public class OutputAudienceMobile implements Serializable{

	private List<MobileAudience> audienceList;
	private String queryID;
	private int audienceSize;
	private int campaignID;
	private String token;
	
	public List<MobileAudience> getAudienceList() {
		return audienceList;
	}
	public void setAudienceList(List<MobileAudience> audienceList) {
		this.audienceList = audienceList;
	}
	public String getQueryID() {
		return queryID;
	}
	public void setQueryID(String queryID) {
		this.queryID = queryID;
	}
	public int getAudienceSize() {
		return audienceSize;
	}
	public void setAudienceSize(int audienceSize) {
		this.audienceSize = audienceSize;
	}
	public int getCampaignID() {
		return campaignID;
	}
	public void setCampaignID(int campaignID) {
		this.campaignID = campaignID;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
	

}
