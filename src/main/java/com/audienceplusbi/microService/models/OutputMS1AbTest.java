package com.audienceplusbi.microService.models;

import java.io.Serializable;
import java.util.List;

public class OutputMS1AbTest implements Serializable{
	
//	{"audienceSize":1,"queryId":"20180607181107207","ctype":"abtest","id":27}
	
	private int abtCode;
	private List<ABTest> abTestAudience;
    private int audienceSize;
    private String queryId;
    private String ctype;
    private int id;
    private String token;
    
    
    
	public int getAbtCode() {
		return abtCode;
	}
	public void setAbtCode(int abtCode) {
		this.abtCode = abtCode;
	}
	public List<ABTest> getAbTestAudience() {
		return abTestAudience;
	}
	public void setAbTestAudience(List<ABTest> abTestAudience) {
		this.abTestAudience = abTestAudience;
	}
	public int getAudienceSize() {
		return audienceSize;
	}
	public void setAudienceSize(int audienceSize) {
		this.audienceSize = audienceSize;
	}
	public String getQueryId() {
		return queryId;
	}
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
    
    
    
	
}
